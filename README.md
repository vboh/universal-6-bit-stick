# Universal 6-bit stick
Vánoční dárek od Viktora Bohuňka pro jeho přátele.
## Myšlenka díla
Darovat něco s čím se dá dál experimentovat. Ať jsou to užitečné šoučástky nebo je to možnost vidět zdrojové kódy a dále je modifikovat. 

Darovat něco, co možná donutí ostatní zamyslet se nad tím, co je zatím, že to bliká?

Nápad většiny prvních programů pochází z hlavy Mr. Q. Mr. Q si nepřál zveřejnit své pravé jméno, ale tímto mu chci znovu poděkovat za podporu v projektu.

## Technická myšlenka
Vzít levné, univerzální a snadno programovatelné zařízení a postavit kolem něj malý ekosystém, který zvládne pochopit a používat téměř každý.

**Díky, že jsi dočetl až sem.**

>Viktor

Nechci se moc rozepisovat o detailech, ty jsou koneckonců v jednotlivých hlavních složkách repozitáře.