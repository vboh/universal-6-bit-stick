<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {      
    if($_FILES['image_file']['error'] == 0) {        
            $bmp_image = fopen($_FILES['image_file']['tmp_name'], 'rb');            

            //Make array from BMP header
            $data = fread($bmp_image, 14);
            $BITMAPFILEHEADER = unpack('A2bfType/'
                    . 'V1bfSize/'
                    . 'v1bfReserved1/'
                    . 'v1bfReserved2/'
                    . 'V1bfOffBits', $data);

            //Test header, is it BMP?
            if($BITMAPFILEHEADER['bfType'] == 'BM') {
                //Make aray from BMP info header
                $data = fread($bmp_image, 40);     
                $BITMAPINFOHEADER = unpack('V1biSize/'
                        . 'V1biWidth/'
                        . 'V1biHeight/'
                        . 'v1biPlanes/'
                        . 'v1biBitCount/'
                        . 'V1biCompression/'
                        . 'V1biSizeImage/'
                        . 'V1biXPelsPerMeter/'
                        . 'V1biYPelsPerMeter/'
                        . 'V1biClrUsed/'
                        . 'V1biClrImportant', $data);

                //Test format of BMP
                if(($BITMAPINFOHEADER['biBitCount'] == 24) && ($BITMAPINFOHEADER['biCompression'] == 0)) {
                    //Test parameters of BMP
                           //Is count of pixels in range                                      AND   Is height at minimum 6 pixels?    AND    Is height dividable by 6?
                    if((($BITMAPINFOHEADER['biWidth'] * $BITMAPINFOHEADER['biHeight']) < 5000) && ($BITMAPINFOHEADER['biHeight'] >= 6) && (($BITMAPINFOHEADER['biHeight'] % 6) == 0)) {

                        //Calculate byte length of row
                        $byte_width = $BITMAPINFOHEADER['biWidth'] * 3;
                        //Byte width must be dividable by 4
                        if(($byte_width % 4) > 0) {
                            $byte_width += (4 - ($byte_width % 4));                
                        }                

                        //Read whole bitmap to array
                        $bitmap_data = fread($bmp_image, $BITMAPINFOHEADER['biSizeImage']);
                        //Prepare data counter
                        $data_bytes = 0;
                        //Temporary data before write to output data
                        $data_temp = 0;
                        //Output data
                        $POVStick_data = [];
                        $POVStick_data_string = '';
                        
                        for ($i = 2; $i < ($BITMAPINFOHEADER['biWidth'] * 3); $i += 3) {
                            for ($j = $BITMAPINFOHEADER['biHeight'] - 1; $j >= 0; $j--) {                        
                                $RGB = unpack("C1R/C1G/C1B", ($bitmap_data[($byte_width * $j) + $i]) .      //R
                                                             ($bitmap_data[($byte_width * $j) + $i - 1]) .  //G
                                                             ($bitmap_data[($byte_width * $j) + $i - 2]));  //B

                                if(!(($RGB['R'] + $RGB['G'] + $RGB['B']) == 0)) { // Černá
                                    $data_temp |= 1 << $j;                                    
                                }
                            }                            
                            $POVStick_data_string = $POVStick_data_string . ',0x' . strtoupper(dechex($data_temp)); 
                            $data_temp = 0;
                            $data_bytes++;
                        }
                        
                        $data_section = '#define LED_DATA_LENGTH ' . $data_bytes .  '
                            
                                static const uint8_t LED_DATA[LED_DATA_LENGTH] PROGMEM = {' . substr_replace($POVStick_data_string, '', 0, 1) . '};';
                        
$first_section = '/**
    POVStick
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

#define TIMER_BOTTOM 126

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//#define LED_DATA_LENGTH 240

volatile uint8_t i, shiftout_data, counter = 4, pwm_counter = 0, data_byte, data_byte_1, data_byte_2, data_byte_3, data_byte_4, data_byte_5;
volatile uint16_t byte = 0;

';
                        
$second_section = '
    
    void shiftOut(uint8_t data) {
    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM;
    if(counter == 4) {
        data_byte = pgm_read_byte(&(LED_DATA[byte]));

        shiftout_data = 0;

        if(data_byte & 0b00000001) {
            shiftout_data = REAL_LED_1;
        }
        if(data_byte & 0b00000010) {
            shiftout_data |= REAL_LED_2;
        }
        if(data_byte & 0b00000100) {
            shiftout_data |= REAL_LED_3;
        }
        if(data_byte & 0b00001000) {
            shiftout_data |= REAL_LED_4;
        }
        if(data_byte & 0b00010000) {
            shiftout_data |= REAL_LED_5;
        }
        if(data_byte & 0b00100000) {
            shiftout_data |= REAL_LED_6;
        }

        shiftOut(shiftout_data);

        byte++;
        if(byte == LED_DATA_LENGTH) {
            byte = 0;
        }

        counter = 0;
    }
    counter++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0

    // Prescale 256
    TCCR0B = 0b00000100; // CS02

    //Enable int
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable ints
    sei();

    while(1) {
    }

    return 0;}
';
                        //Send complete file to user
                        $tmpName = tempnam("tmp", 'main');
                        $file = fopen($tmpName, 'w');
                        
                        $file_string = $first_section . $data_section . $second_section;
                        
                        fwrite($file, $file_string);
                        fclose($file);

                        header('Content-Description: File Transfer');
                        header('Content-Type: text/x-csrc');                        
                        header('Content-Disposition: attachment; filename=main.c');                        
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($tmpName));

                        //ob_clean();
                        //flush();
                        readfile($tmpName);

                        unlink($tmpName);
                        exit();
                    } else {
                        $error = 'Obrázek nemá správné rozměry';
                    }
                } else {
                    $error = 'Nepodporovaný formát BMP';
                }
            } else {
                $error = 'Soubor není formátu BMP';
            }
        fclose($bmp_image);
    } else {
        $error = 'Nebyl nahrán žádný soubor';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>U6bS - Programy - POVStick</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link href="css/own_style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_main" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">U6bS</a>

                </div>
                <div class="collapse navbar-collapse" id="navbar_main">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">O projektu</a></li>
                        <li><a href="oprogramech.html">O programech</a></li>
                        <li class="active dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programy <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="6-bit-casovac.php">6-bit časovač</a></li>
                                <li><a href="6-nezavislych-casovacu.php">6 nezávislých časovačů</a></li>
                                <li><a href="sesti-usekovy-odpocitavac.php">Šesti úsekový odpočítávač</a></li>
                                <li><a href="behajici-led.php">Běhající LED</a></li>
                                <li><a href="morseovka.php">Morseovka</a></li>
                                <li class="active"><a href="povstick.php">POVStick</a></li>
                            </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>POVStick</h1>        
                </div>
            </div>
            
            <?php
                if(isset($error)) {
                    ?>  <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Chyba:</span>
                                    <?php echo $error; ?>
                                </div> 
                            </div>
                        </div> <?php
                }           
            ?>            
            
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <label for="image_file">Vstupní BMP</label>
                                <input name="image_file" id="image_file" type="file">
                            </div>
                            <button type="submit" class="btn btn-primary">Odeslat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            
            <footer class="footer">
                <div class="container">
                    <div class="text-muted text-center">&COPY; Viktor Bohuněk 2016</div>
                </div>
            </footer>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
    </body>
</html>














