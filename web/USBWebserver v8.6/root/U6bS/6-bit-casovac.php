<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $perioda = filter_input(INPUT_POST, 'perioda');
    $smer_citani = filter_input(INPUT_POST, 'smer_citani');
    if (!empty($perioda)) {
        if (filter_var($perioda, FILTER_VALIDATE_FLOAT)) {
            if((($perioda <= 274877) && ($perioda >= 0.000064))) {
            if (!empty($smer_citani)) {                                
                $data_section = '#define TIMEOUT ' . ceil(15625 * $perioda) . '
                    
                        ';
                

                $data_sectiom_up = '
#define DATA_LOOPING if(data > 63) {data = 0;}
#define DATA_INCREMENT data++;
#define DATA_INIT 0
';

                $data_sectiom_down = '
#define DATA_INCREMENT data--;
#define DATA_LOOPING if(data == 0) {data = 63;}
#define DATA_INIT 63
';

                if($smer_citani == "dolu") {
                    $data_section = $data_section . $data_sectiom_down;
                } else if($smer_citani == "nahoru") {
                    $data_section = $data_section . $data_sectiom_up;
                }
                
                $first_section = '/**
    6 bit timer
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000
';

                $second_section = '//Variables for counting LED timeout
volatile uint32_t time = 0;

//Shiftout data; counter in for
volatile uint8_t data = DATA_INIT, i, data_shift_out;

//Shift data to register
void shiftOut(uint8_t data) {
    data_shift_out = 0;
    if(data & 0b00000001) {
        data_shift_out |= REAL_LED_1;
    }
    if(data & 0b00000010) {
        data_shift_out |= REAL_LED_2;
    }
    if(data & 0b00000100) {
        data_shift_out |= REAL_LED_3;
    }
    if(data & 0b00001000) {
        data_shift_out |= REAL_LED_4;
    }
    if(data & 0b00010000) {
        data_shift_out |= REAL_LED_5;
    }
    if(data & 0b00100000) {
        data_shift_out |= REAL_LED_6;
    }

    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data_shift_out & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

//Every tick of timer
ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM; //Set CTC bottom
    if(time == TIMEOUT) {
        DATA_INCREMENT
        shiftOut(data); //Shift out data to register
        time = 0;
        DATA_LOOPING
    }
    time++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    // Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable interrupt
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable interrupts
    sei();

    while(1) {
        //NOTHING :D
    }

    return 0;}';
                
            $file_string = $first_section . $data_section . $second_section;

//Send complete file to user
            $tmpName = tempnam("tmp", 'main');
            $file = fopen($tmpName, 'w');

            fwrite($file, $file_string);
            fclose($file);

            header('Content-Description: File Transfer');
            header('Content-Type: text/x-csrc');
            header('Content-Disposition: attachment; filename=main.c');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tmpName));

            readfile($tmpName);

            unlink($tmpName);
            
            exit();
            
            } else {
                $error = 'Nebyl zadán směr čítání';
            }
        } else {
            $error = 'Číslo je mimo rozsah <274877; 0,000064>';
        }
        } else {
            $error = 'Nebylo zadáno číslo';
        }
    } else {
        $error = 'Nebyla odeslána žádná hodnota';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>U6bS - Programy - 6-bit časovač</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link href="css/own_style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_main" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">U6bS</a>

                </div>
                <div class="collapse navbar-collapse" id="navbar_main">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">O projektu</a></li>
                        <li><a href="oprogramech.html">O programech</a></li>
                        <li class="active dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programy <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="active"><a href="6-bit-casovac.php">6-bit časovač</a></li>
                                <li><a href="6-nezavislych-casovacu.php">6 nezávislých časovačů</a></li>
                                <li><a href="sesti-usekovy-odpocitavac.php">Šesti úsekový odpočítávač</a></li>
                                <li><a href="behajici-led.php">Běhající LED</a></li>
                                <li><a href="morseovka.php">Morseovka</a></li>
                                <li><a href="povstick.php">POVStick</a></li>
                            </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>6-bit časovač</h1>        
                </div>
            </div>

            <?php
            if (isset($error)) {
                ?>  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Chyba:</span>
                            <?php echo $error; ?>
                        </div> 
                    </div>
                </div> <?php
            }
            ?>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form method="post" action="">
                        <div class="form-group">
                            <label for="perioda">Perioda</label>
                            <div class="input-group">	
                                <input type="number" name="perioda" id="perioda" class="form-control" aria-describedby="sekundy" step="any">
                                <span class="input-group-addon" id="sekundy">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <lable>Směr čítání</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="smer_citani" id="nahoru" value="nahoru" checked>
                                        Nahoru
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="smer_citani" id="dolu" value="dolu">
                                        Dolů
                                    </label>
                                </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Odeslat</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>            
        </div>
        
        <footer class="footer">
            <div class="container">
                <div class="text-muted text-center">&COPY; Viktor Bohuněk 2016</div>
            </div>
        </footer>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>