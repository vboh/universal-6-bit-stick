<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//Test na vyplnění formuláře
    if (filter_input(INPUT_POST, 'text') != '') {
        $text_puvodni = filter_input(INPUT_POST, 'text');
        $text = strtolower($text_puvodni);
        if (preg_match_all('/[^a-z0-9 ]/', $text) == 0) {
            $morse_code = array(
                'a' => '.-',
                'b' => '-...',
                'c' => '-.-.',
                'd' => '-..',
                'e' => '.',
                'f' => '..-.',
                'g' => '--.',
                'h' => '....',
                'i' => '..',
                'j' => '.---',
                'k' => '-.-',
                'l' => '.-..',
                'm' => '--',
                'n' => '-.',
                'o' => '---',
                'p' => '.--.',
                'q' => '--.-',
                'r' => '.-.',
                's' => '...',
                't' => '-',
                'u' => '..-',
                'v' => '...-',
                'w' => '.--',
                'x' => '-..-',
                'y' => '-.--',
                'z' => '--..',
                '0' => '-----',
                '1' => '.----',
                '2' => '..---',
                '3' => '...--',
                '4' => '....-',
                '5' => '.....',
                '6' => '-....',
                '7' => '--...',
                '8' => '---..',
                '9' => '----.',
                ' ' => '0'
            );

            $byte_string = '';

            for ($i = 0; $i < strlen($text); $i++) {
                $symbol = $morse_code[$text[$i]];
                for ($j = 0; $j < strlen($symbol); $j++) {
                    switch ($symbol[$j]) {
                        case '.' : {
                                $byte_string = $byte_string . '1';
                                if ($j < (strlen($symbol) - 1)) {
                                    $byte_string = $byte_string . '0';
                                }
                                break;
                            }

                        case '-' : {
                                $byte_string = $byte_string . '111';
                                if ($j < (strlen($symbol) - 1)) {
                                    $byte_string = $byte_string . '0';
                                }
                                break;
                            }

                        case '0' : {
                                $byte_string = $byte_string . '00';
                                break;
                            }
                    }
                }
                if ($i < (strlen($text) - 1)) {
                    $byte_string = $byte_string . '00';
                } else {
                    $nul = 8 - (strlen($byte_string) % 8);
                    if ($nul < 8) {
                        $byte_string = $byte_string . substr('00000000000000', 0, ($nul + 8));
                    } else {
                        $byte_string = $byte_string . substr('00000000', 0, ($nul));
                    }
                }
            }

            $byte_array = array();

            for ($k = 0; $k < (strlen($byte_string) / 8); $k++) {
                array_push($byte_array, 0);
            }

            $bit = 0;
            $byte = 0;
            for ($i = 0; $i < strlen($byte_string); $i++) {
                $byte_array[$byte] = ($byte_array[$byte] << 1);
                if ($byte_string[$i] == '1') {
                    $byte_array[$byte] |= 1;
                }
                $bit++;
                if ($bit > 7) {
                    $byte++;
                    $bit = 0;
                }
            }

            $first_section = '/**
    Morse code
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

volatile uint8_t i, time_counter = 0, shift_data = 0, output_data = 0, bit = 0;
volatile uint16_t byte = 0; ';

            $second_section = '/**
Mapuje LED zapsané v bytu stylem první LED na zařízení = bit s váhou 1
Př.:
Vstup 0b00000001 převede na 0b01000000
*/
uint8_t remap_leds(uint8_t data) {
    output_data = 0;
    if(data & 0b00000001) { //Je na pozici označené jedničkou jednička i ve vstupním bitu?
        output_data = REAL_LED_1;
    }

    if(data & 0b00000010) {
        output_data |= REAL_LED_2;
    }

    if(data & 0b00000100) {
        output_data |= REAL_LED_3;
    }

    if(data & 0b00001000) {
        output_data |= REAL_LED_4;
    }

    if(data & 0b00010000) {
        output_data |= REAL_LED_5;
    }

    if(data & 0b00100000) {
        output_data |= REAL_LED_6;
    }
    return output_data;
}

void shiftOut(uint8_t data) {

    data = remap_leds(data); //Provedeme přemapování LEDek

    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    if(time_counter == 62) { //Odměříme cca. sekundu
        if((bit < 6) && (byte == 0)) { //Prvních šest bitů celé sekvence si vyžaduje speciální přístup
            //Z prostoru programové paměti přečtme byte a provedem posun, ten zajistí hladký náběh sekvence
            shift_data = (pgm_read_byte(&(DATA[byte])) >> (7 - bit));
        } else { //Ostatní byty
            shift_data = (shift_data << 1); //Posuneme to co už je na
            //Operace zjistí jestli v bitu, který se bude nově zpbrazovat je jednička
            if(pgm_read_byte(&(DATA[byte])) & (0x80 >> bit)) {
                shift_data |= 0b00000001; //Pokud je, na konec sekvece se přidá jednička
            }
        }
        bit++; //Další bit prosím
        if(bit == 8) { //Maximum je sedm...
            bit = 0;
            byte++; //Nový byte
            if(byte == DATA_LENGTH) { //Víc než maximum jich nemáme
                byte = 0;
            }
        }
        shiftOut(shift_data); //Pošleme novou skevenci na zobrazovadlo
        time_counter = 0; //Čas budeme počítat od nuly
    }
    time_counter++;
}

int main(void) {
    //Inicializace pinů
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Inicializace čítače 0

    //Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Povolit přerušení časovače
    TIMSK = 0b00000010; //TOIE0

    //Povolit přerušení
    sei();

    while(1) {
    }

    return 0;}';

            $data_section = '

        #define DATA_LENGTH ' . $byte . '

        static const uint8_t DATA[DATA_LENGTH] PROGMEM = {';

            foreach ($byte_array as $value) {
                $data_section = $data_section . '0x' . dechex($value) . ',';
            }

            $data_section = rtrim($data_section, ',');
            
            $data_section = $data_section . '};

        ';
        
            $file_string = $first_section . $data_section . $second_section;

//Send complete file to user
            $tmpName = tempnam("tmp", 'main');
            $file = fopen($tmpName, 'w');

            fwrite($file, $file_string);
            fclose($file);

            header('Content-Description: File Transfer');
            header('Content-Type: text/x-csrc');
            header('Content-Disposition: attachment; filename=main.c');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tmpName));

            readfile($tmpName);

            unlink($tmpName);     
            
            exit();
            
        } else {
            $error = 'Nalezeny nepovolené znaky. Použít je možné pouze znaky abecedy, čísla a mezeru';
        }
    } else {
        $error = 'Nebyl zadán žádný text';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>U6bS - Programy - Morseovka</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link href="css/own_style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_main" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">U6bS</a>

                </div>
                <div class="collapse navbar-collapse" id="navbar_main">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">O projektu</a></li>
                        <li><a href="oprogramech.html">O programech</a></li>
                        <li class="active dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programy <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="6-bit-casovac.php">6-bit časovač</a></li>
                                <li><a href="6-nezavislych-casovacu.php">6 nezávislých časovačů</a></li>
                                <li><a href="sesti-usekovy-odpocitavac.php">Šesti úsekový odpočítávač</a></li>
                                <li><a href="behajici-led.php">Běhající LED</a></li>                                
                                <li class="active"><a href="morseovka.php">Morseovka</a></li>
                                <li><a href="povstick.php">POVStick</a></li>
                            </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Morseovka</h1>        
                </div>
            </div>
            
            <?php
                if(isset($error)) {
                    ?>  <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Chyba:</span>
                                    <?php echo $error; ?>
                                </div> 
                            </div>
                        </div> <?php
                }           
            ?>
            
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="text">Text</label>
                            <div>
                                <textarea rows="6" cols="50" name="text" id="text"><?php if(isset($text_puvodni)) {echo $text_puvodni;} ?></textarea>                                
                            </div>
                            <button type="submit" class="btn btn-primary">Odeslat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            
        <footer class="footer">
            <div class="container">
                <div class="text-muted text-center">&COPY; Viktor Bohuněk 2016</div>
            </div>
        </footer>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
    </body>
</html>














