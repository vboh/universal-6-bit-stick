<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $leds = array('LED1' => 0,'LED2' => 0,'LED3' => 0,'LED4' => 0,'LED5' => 0,'LED6' => 0);
    $leds['LED1'] = filter_input(INPUT_POST, 'led1');
    $leds['LED2'] = filter_input(INPUT_POST, 'led2');
    $leds['LED3'] = filter_input(INPUT_POST, 'led3');
    $leds['LED4'] = filter_input(INPUT_POST, 'led4');
    $leds['LED5'] = filter_input(INPUT_POST, 'led5');
    $leds['LED6'] = filter_input(INPUT_POST, 'led6');
    $sub_error_empty = '';    
    $sub_error_out = '';    
    
    foreach ($leds as $key => $led) {
        if (empty($led)) {        
            $sub_error_empty = $sub_error_empty . substr_replace(($key . ', '), ' ', 3, 0);
        } else {
            if(!(($led <= 274877) && ($led >= 0.000064))) {
                $sub_error_out = $sub_error_out . substr_replace(($key . ', '), ' ', 3, 0);
            }
        }
    }
       
    if(empty($sub_error_empty) && empty($sub_error_out)) {
    $data_section = '';
        
        foreach ($leds as $key => $led) {
            $data_section = $data_section . '#define ' . $key . '_TIMEOUT ' . ceil(15625 * $led) . '
                    
                        ';
        }
        
        $first_section = '/**
    Universal-6-independent-bit-timer
    Copyright (C) 2016 Viktor BohunÃ¬k

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//LED timeouts, formula: 16000000/1024 * seconds

';
        $second_section = '

//Variables for counting LED timeout
volatile uint32_t   led1 = 0,
                    led2 = 0,
                    led3 = 0,
                    led4 = 0,
                    led5 = 0,
                    led6 = 0;

                    //Shiftout data; counter in for
volatile uint8_t data = 0, i;

//Shift data to register
void shiftOut(uint8_t data) {
    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

//Every tick of timer
ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM; //Set CTC bottom
    if(led1 == LED1_TIMEOUT) { //If timeout
        data ^= REAL_LED_1; //Switch state of LED
        led1 = 0; //Reset timeout counter
    }
    if(led2 == LED2_TIMEOUT) {
        data ^= REAL_LED_2;
        led2 = 0;
    }
    if(led3 == LED3_TIMEOUT) {
        data ^= REAL_LED_3;
        led3 = 0;
    }
    if(led4 == LED4_TIMEOUT) {
        data ^= REAL_LED_4;
        led4 = 0;
    }
    if(led5 == LED5_TIMEOUT) {
        data ^= REAL_LED_5;
        led5 = 0;
    }
    if(led6 == LED6_TIMEOUT) {
        data ^= REAL_LED_6;
        led6 = 0;
    }
    led1++; //Increment timeout counter
    led2++;
    led3++;
    led4++;
    led5++;
    led6++;
    shiftOut(data); //Shift out data to regoster
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0
    // Prescale 1024 - USED
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable interrupt
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable interrupts
    sei();

    while(1) {
        //NOTHING :D
    }
    return 0;}
';
        
        $file_string = $first_section . $data_section . $second_section;

//Send complete file to user
            $tmpName = tempnam("tmp", 'main');
            $file = fopen($tmpName, 'w');

            fwrite($file, $file_string);
            fclose($file);

            header('Content-Description: File Transfer');
            header('Content-Type: text/x-csrc');
            header('Content-Disposition: attachment; filename=main.c');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tmpName));

            readfile($tmpName);

            unlink($tmpName);

            exit();
        
    } else {        
        if((!empty($sub_error_empty)) && (!empty($sub_error_out))) {
            $error = 'Nebyla odeslána hodnota ' . substr($sub_error_empty, 0, -2) . '<br><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Chyba:</span>' . ' Hodnota ' . substr($sub_error_out, 0, -2) . ' je mimo rozsah <274877; 0,000064>';
        } else {            
            if(!empty($sub_error_empty)) {
                $error = 'Nebyla odeslána hodnota ' . substr($sub_error_empty, 0, -2);
            }
            if(!empty($sub_error_out)) {
                $error = 'Hodnota ' . substr($sub_error_out, 0, -2) . ' je mimo rozsah <274877; 0,000064>';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>U6bS - Programy - 6 nezávislých časovačů</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link href="css/own_style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_main" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">U6bS</a>

                </div>
                <div class="collapse navbar-collapse" id="navbar_main">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">O projektu</a></li>
                        <li><a href="oprogramech.html">O programech</a></li>
                        <li class="active dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programy <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="6-bit-casovac.php">6-bit časovač</a></li>
                                <li class="active"><a href="6-nezavislych-casovacu.php">6 nezávislých časovačů</a></li>
                                <li><a href="sesti-usekovy-odpocitavac.php">Šesti úsekový odpočítávač</a></li>
                                <li><a href="behajici-led.php">Běhající LED</a></li>
                                <li><a href="morseovka.php">Morseovka</a></li>
                                <li><a href="povstick.php">POVStick</a></li>
                            </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>6 nezávislých časovačů</h1>        
                </div>
            </div>
            
            <?php
            if (isset($error)) {
                ?>  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Chyba:</span>
                            <?php echo $error; ?>
                        </div> 
                    </div>
                </div> <?php
            }
            ?>
            
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form method="post" action="">
                        <div class="form-group">
                            <label for="led1">LED 1</label>
                            <div class="input-group">
                                <input type="number" name="led1" id="led1" class="form-control" aria-describedby="sekundy1" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED1'] . "'";} ?> >            
                                <span class="input-group-addon" id="sekundy1">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="led2">LED 2</label>
                            <div class="input-group">
                                <input type="number" name="led2" id="led2" class="form-control" aria-describedby="sekundy2" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED2'] . "'";} ?>>            
                                <span class="input-group-addon" id="sekundy2">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="led3">LED 3</label>
                            <div class="input-group">
                                <input type="number" name="led3" id="led3" class="form-control" aria-describedby="sekundy3" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED3'] . "'";} ?>>            
                                <span class="input-group-addon" id="sekundy3">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="led4">LED 4</label>
                            <div class="input-group">
                                <input type="number" name="led4" id="led4" class="form-control" aria-describedby="sekundy4" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED4'] . "'";} ?>>            
                                <span class="input-group-addon" id="sekundy4">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="led5">LED 5</label>
                            <div class="input-group">
                                <input type="number" name="led5" id="led5" class="form-control" aria-describedby="sekundy5" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED5'] . "'";} ?>>            
                                <span class="input-group-addon" id="sekundy5">s</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="led6">LED 6</label>
                            <div class="input-group">
                                <input type="number" name="led6" id="led6" class="form-control" aria-describedby="sekundy6" step="any" <?php if (isset($error)) { echo " value='" . $leds['LED6'] . "'";} ?>>            
                                <span class="input-group-addon" id="sekundy6">s</span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Odeslat</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>            
        </div>
        
        <footer class="footer">
            <div class="container">
                <div class="text-muted text-center">&COPY; Viktor Bohuněk 2016</div>
            </div>
        </footer>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>














