<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {      
    if($_FILES['image_file']['error'] == 0) {        
            $bmp_image = fopen($_FILES['image_file']['tmp_name'], 'rb');            

            //Make array from BMP header
            $data = fread($bmp_image, 14);
            $BITMAPFILEHEADER = unpack('A2bfType/'
                    . 'V1bfSize/'
                    . 'v1bfReserved1/'
                    . 'v1bfReserved2/'
                    . 'V1bfOffBits', $data);

            //Test header, is it BMP?
            if($BITMAPFILEHEADER['bfType'] == 'BM') {
                //Make aray from BMP info header
                $data = fread($bmp_image, 40);     
                $BITMAPINFOHEADER = unpack('V1biSize/'
                        . 'V1biWidth/'
                        . 'V1biHeight/'
                        . 'v1biPlanes/'
                        . 'v1biBitCount/'
                        . 'V1biCompression/'
                        . 'V1biSizeImage/'
                        . 'V1biXPelsPerMeter/'
                        . 'V1biYPelsPerMeter/'
                        . 'V1biClrUsed/'
                        . 'V1biClrImportant', $data);

                //Test format of BMP
                if(($BITMAPINFOHEADER['biBitCount'] == 24) && ($BITMAPINFOHEADER['biCompression'] == 0)) {
                    //Test parameters of BMP
                           //Is count of pixels in range                                      AND   Is height at minimum 6 pixels?    AND    Is height dividable by 6?
                    if((($BITMAPINFOHEADER['biWidth'] * $BITMAPINFOHEADER['biHeight']) < 5000) && ($BITMAPINFOHEADER['biHeight'] >= 6) && (($BITMAPINFOHEADER['biHeight'] % 6) == 0)) {

                        //Calculate byte length of row
                        $byte_width = $BITMAPINFOHEADER['biWidth'] * 3;
                        //Byte width must be dividable by 4
                        if(($byte_width % 4) > 0) {
                            $byte_width += (4 - ($byte_width % 4));                
                        }                

                        //Read whole bitmap to array
                        $bitmap_data = fread($bmp_image, $BITMAPINFOHEADER['biSizeImage']);
                        //Prepare data counter
                        $data_position = 0;
                        //Temporary data beofre write to output data
                        $data_temp = 0;
                        //Output data
                        $POVStick_data = [];
                        $POVStick_data_string = "";

                        for ($i = 2; $i < ($BITMAPINFOHEADER['biWidth'] * 3); $i += 3) {
                            for ($j = $BITMAPINFOHEADER['biHeight'] - 1; $j >= 0; $j--) {                        
                                $RGB = unpack("C1R/C1G/C1B", ($bitmap_data[($byte_width * $j) + $i]) .      //R
                                                             ($bitmap_data[($byte_width * $j) + $i - 1]) .  //G
                                                             ($bitmap_data[($byte_width * $j) + $i - 2]));  //B

                                if(($RGB['R'] == $RGB['G']) && ($RGB['G'] == $RGB['B'] && ((($RGB['G'] % 16) == 0) || ($RGB['G'] == 255)))) { // Odstín šedi nebo bílá
                                    if($RGB['G'] == 255) { //Bílá = 15
                                        $value = 15;                                
                                    } else {
                                        $value = ($RGB['G'] / 16) - 1; //Odstín šedi namapovat na 15 hodnot
                                        if($value < 0) { //Černá = nula
                                            $value = 0;
                                        }         
                                    }
                                } else { //Ostatní barvy = bílá
                                    $value = 15;
                                }                               
                                
                                if(($data_position % 2) == 0) { // Data na sudé pozici
                                    $data_temp = $value << 4; //Posun o 4 bity
                                } else { // Data na liché pozici
                                    $data_temp += $value; //Přičíst k datům na sudé                            
                                    //array_push($POVStick_data, $data_temp); //Vložit do pole
                                    $POVStick_data_string = $POVStick_data_string . ',0x' . strtoupper(dechex($data_temp));
                                }
                                $data_position++; //Upravit pozici dat
                            }
                        }
                        
                        //Delete first colon 
                        $POVStick_data_string = substr_replace($POVStick_data_string, '', 0, 1);
                        
                        $POVStick_data_string = '#define LED_DATA_LENGTH ' . (($data_position) / 2) .  "\n" . 'static const uint8_t LED_DATA[LED_DATA_LENGTH] PROGMEM = {' . $POVStick_data_string . '};';
                       
                        //Send complete file to user
                        $tmpName = tempnam(sys_get_temp_dir(), 'main');
                        $file = fopen($tmpName, 'w');

                        fwrite($file, $POVStick_data_string);
                        fclose($file);

                        header('Content-Description: File Transfer');
                        //header('Content-Type: text/x-csrc');
                        header('Content-Type: text');
                        //header('Content-Disposition: attachment; filename=main.c');
                        header('Content-Disposition: attachment; filename=main.txt');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($tmpName));

                        //ob_clean();
                        //flush();
                        readfile($tmpName);

                        unlink($tmpName);
                     
                        $success = 'All OK &#9786;';

                    } else {
                        $error =  'Obrázek nemá správné rozměry';
                    }
                } else {
                    $error = 'Nepodporovaný formát BMP';
                }
            } else {
                $error = 'Soubor není formátu BMP';
            }
        fclose($bmp_image);
    } else {
        $error = 'Nebyl nahrán žádný soubor';
    }
} else {
    $no_data = "OK";    
}

if(isset($no_data) || isset($error)) {
?>  
<!DOCTYPE html>
<html>
    <head>
        <title>Konvertor dat pro POVStick</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript">
            function change() {
                document.getElementById("centering").setAttribute("style", "height: " + window.innerHeight + "px;");
            }    
            function easteregg() {
                document.getElementById("footer").innerHTML = "Viktor Bohuněk &hearts; U";
            }
            function eastereggOff() {
                document.getElementById("footer").innerHTML = "&COPY; Viktor Bohuněk 2016";
            }
            function  main() {
                change();                
                window.onresize = change;
                document.getElementById("footer").onmouseenter = easteregg;
                document.getElementById("footer").onmouseout = eastereggOff;
            }            
            window.onload = main;
        </script>
        <link href="style.css" type="text/css" rel="stylesheet">
    </head>
    <body style="margin-top: -20px;">
        <div id="centering">
            <div id="main">
                <div class="inside cursor_normal">Konvertor dat pro POVStick</div>                
                    <?php 
                    if(isset($error)) {
                        ?><div class="inside cursor_normal error"> <?php echo $error; ?> </div> <?php
                    }
                    ?>                         
                <form method="POST" action="" enctype="multipart/form-data">
                    <div class="inside"><input type="file" name="image_file"></div>
                    <div class="inside"><input type="submit" value="Přenech to magii..."></div>
                </form>
                <div class="inside no_margin cursor_normal"><a href="jak-vytvorit-data.html">Jak si připravit vlastní data?</a></div>
            </div>
            <div id="footer" class="cursor_normal">
                &COPY; Viktor Bohuněk 2016
            </div>
        </div>
    </body>
</html>
<?php } ?>