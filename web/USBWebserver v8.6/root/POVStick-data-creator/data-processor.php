<?php
    $bmp_image = fopen($_FILES["image_file"]["tmp_name"], "rb");
    
    //Make array from BMP header
    $data = fread($bmp_image, 14);
    $BITMAPFILEHEADER = unpack("A2bfType/"
            . "V1bfSize/"
            . "v1bfReserved1/"
            . "v1bfReserved2/"
            . "V1bfOffBits", $data);
    
    //Test header, is it BMP?
    if($BITMAPFILEHEADER['bfType'] == "BM") {
        //Make aray from BMP info header
        $data = fread($bmp_image, 40);     
        $BITMAPINFOHEADER = unpack("V1biSize/"
                . "V1biWidth/"
                . "V1biHeight/"
                . "v1biPlanes/"
                . "v1biBitCount/"
                . "V1biCompression/"
                . "V1biSizeImage/"
                . "V1biXPelsPerMeter/"
                . "V1biYPelsPerMeter/"
                . "V1biClrUsed/"
                . "V1biClrImportant", $data);
        
        //Test format of BMP
        if(($BITMAPINFOHEADER['biBitCount'] == 24) && ($BITMAPINFOHEADER['biCompression'] == 0)) {
            //Test parameters of BMP
                   //Is count of pixels in range                                      AND   Is height at minimum 6 pixels?    AND    Is height dividable by 6?
            if((($BITMAPINFOHEADER['biWidth'] * $BITMAPINFOHEADER['biHeight']) < 2000) && ($BITMAPINFOHEADER['biHeight'] >= 6) && (($BITMAPINFOHEADER['biHeight'] % 6) == 0)) {
                             
                //Calculate byte length of row
                $byte_width = $BITMAPINFOHEADER['biWidth'] * 3;
                //Byte width must be dividable by 4
                if(($byte_width % 4) > 0) {
                    $byte_width += (4 - ($byte_width % 4));                
                }                
                
                //Read whole bitmap to array
                $bitmap_data = fread($bmp_image, $BITMAPINFOHEADER['biSizeImage']);
                //Prepare data counter
                $data_position = 0;
                //Temporary data beofre write to output data
                $data_temp = 0;
                //Output data
                $POVStick_data = [];
                
                for ($i = 2; $i < ($BITMAPINFOHEADER['biWidth'] * 3); $i += 3) {
                    for ($j = $BITMAPINFOHEADER['biHeight'] - 1; $j >= 0; $j--) {                        
                        $RGB = unpack("C1R/C1G/C1B", ($bitmap_data[($byte_width * $j) + $i]) .      //R
                                                     ($bitmap_data[($byte_width * $j) + $i - 1]) .  //G
                                                     ($bitmap_data[($byte_width * $j) + $i - 2]));  //B
                        
                        if(($RGB['R'] == $RGB['G']) && ($RGB['G'] == $RGB['B'] && ((($RGB['G'] % 16) == 0) || ($RGB['G'] == 255)))) { // Odstín šedi nebo bílá
                            if($RGB['G'] == 255) { //Bílá = 15
                                $value = 15;                                
                            } else {
                                $value = ($RGB['G'] / 16) - 1; //Odstín šedi namapovat na 15 hodnot
                                if($value < 0) { //Černá = nula
                                    $value = 0;
                                }         
                            }
                        } else { //Ostatní barvy = bílá
                            $value = 15;
                        }   
                        
                        if(($data_position % 2) == 0) { // Data na sudé pozici
                            $data_temp = $value << 4; //Posun o 4 bity
                        } else { // Data na liché pozici
                            $data_temp += $value; //Přičíst k datům na sudé                            
                            array_push($POVStick_data, $data_temp); //Vložit do pole
                        }
                        $data_position++; //Upravit pozici dat
                    }
                }
                
                
                //Send complete file to user
                $tmpName = tempnam(sys_get_temp_dir(), 'main');
                $file = fopen($tmpName, 'w');

                fwrite($file, "ONly testing. BLA BLA Nah NAh");
                fclose($file);

                header('Content-Description: File Transfer');
                header('Content-Type: text/x-csrc');
                header('Content-Disposition: attachment; filename=main.c');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($tmpName));

                //ob_clean();
                //flush();
                readfile($tmpName);

                unlink($tmpName);
                
                $success = "All OK &#9786;";
                
            } else {
                $error =  'Unsupported dimensions';
            }
        } else {
            $error = 'Unsupported BMP format';
        }
    } else {
        $error = "Not BMP";
    }
    fclose($bmp_image);
    ?>  
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>POVStick data creator</title>
    </head>
    <body>
        
    </body>
</html>
