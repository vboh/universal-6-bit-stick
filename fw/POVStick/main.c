/**
    POVStick
    Copyright (C) 2016 Viktor Bohuněk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

#define TIMER_BOTTOM 126

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//#define LED_DATA_LENGTH 240

volatile uint8_t i, shiftout_data, counter = 4, pwm_counter = 0, data_byte, data_byte_1, data_byte_2, data_byte_3, data_byte_4, data_byte_5;
volatile uint16_t byte = 0;

#define LED_DATA_LENGTH 7
static const uint8_t LED_DATA[LED_DATA_LENGTH] PROGMEM = {
    //0b00100001, 0b00010010, 0b00111111, 0b00101101, 0b00111111, 0b00101101, 0b00100001, 0b00010010, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    0b00011111, 0b00100100, 0b00100100, 0b00100100, 0b00011111, 0x00, 0x00
};

void shiftOut(uint8_t data) {
    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM;
    if(counter == 4) {
        data_byte = pgm_read_byte(&(LED_DATA[byte]));

        shiftout_data = 0;

        if(data_byte & 0b00000001) {
            shiftout_data = REAL_LED_1;
        }
        if(data_byte & 0b00000010) {
            shiftout_data |= REAL_LED_2;
        }
        if(data_byte & 0b00000100) {
            shiftout_data |= REAL_LED_3;
        }
        if(data_byte & 0b00001000) {
            shiftout_data |= REAL_LED_4;
        }
        if(data_byte & 0b00010000) {
            shiftout_data |= REAL_LED_5;
        }
        if(data_byte & 0b00100000) {
            shiftout_data |= REAL_LED_6;
        }

        shiftOut(shiftout_data);

        byte++;
        if(byte == LED_DATA_LENGTH) {
            byte = 0;
        }

        counter = 0;
    }
    counter++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0

    // Prescale 1
    //TCCR0B = 0b00000001; // CS00
    // Prescale 8
    //TCCR0B = 0b00000010; // CS01
    // Prescale 64
    //TCCR0B = 0b00000011; // CS01 CS00
    // Prescale 256
    TCCR0B = 0b00000100; // CS02
    // Prescale 1024
    //TCCR0B = 0b00000101; // CS02 CS00



    //Enable int
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable ints
    sei();

    while(1) {
    }

    return 0;
}
