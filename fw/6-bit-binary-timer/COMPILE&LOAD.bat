::Kvuli prehlednosti vystupu jsou vypnuta hlaseni
@ECHO OFF
ECHO Please, turn your POVStick off and disconnect it from PC
@ PAUSE
::Spusti kompilator
..\avr8-gnu-toolchain\bin\avr-gcc.exe -Wall -Os -DF_CPU=16000000UL -mmcu=attiny85 -c main.c -o main.o
..\avr8-gnu-toolchain\bin\avr-gcc.exe -Wall -Os -DF_CPU=16000000UL -mmcu=attiny85 -o main.elf main.o
::Zobrazi, kolik mista zabira program
..\avr8-gnu-toolchain\bin\avr-size.exe --mcu=attiny85 --format=avr main.elf
::Vytvori soubor se strojovym kodem v binarni podobe
..\avr8-gnu-toolchain\bin\avr-objcopy.exe -R .eeprom -R .fuse -R .lock -R .signature -O ihex main.elf main.hex
::Program pro nahravani strojoveho kodu do POVSticku (Digispark ATTINY85 DEVBOARD)
..\micronucleus.exe --run main.hex
::Finalni uklid
DEL main.o
DEL main.hex
DEL main.elf
@PAUSE

