/**
    6 bit timer
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//Timeout, formula: 16000000/1024 * seconds
#define TIMEOUT 15625

#define DATA_LOOPING if(data > 63) {data = 0;}
#define DATA_INCREMENT data++;
#define DATA_INIT 0

/*
#define DATA_INCREMENT data--;
#define DATA_LOOPING if(data == 0) {data = 63;}
#define DATA_INIT 63
*/

//Variables for counting LED timeout
volatile uint32_t time = 0;

//Shiftout data; counter in for
volatile uint8_t data = DATA_INIT, i, data_shift_out;

//Shift data to register
void shiftOut(uint8_t data) {
    data_shift_out = 0;
    if(data & 0b00000001) {
        data_shift_out |= REAL_LED_1;
    }
    if(data & 0b00000010) {
        data_shift_out |= REAL_LED_2;
    }
    if(data & 0b00000100) {
        data_shift_out |= REAL_LED_3;
    }
    if(data & 0b00001000) {
        data_shift_out |= REAL_LED_4;
    }
    if(data & 0b00010000) {
        data_shift_out |= REAL_LED_5;
    }
    if(data & 0b00100000) {
        data_shift_out |= REAL_LED_6;
    }

    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data_shift_out & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

//Every tick of timer
ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM; //Set CTC bottom
    if(time == TIMEOUT) {
        DATA_INCREMENT
        shiftOut(data); //Shift out data to register
        time = 0;
        DATA_LOOPING
    }
    time++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    // Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable interrupt
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable interrupts
    sei();

    while(1) {
        //NOTHING :D
    }

    return 0;
}
