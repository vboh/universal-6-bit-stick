# 6 bit timer

## Popis
Firmware umí odpočítat 64 časových úseků, číslo úseku ve kterém se nachází zobrazuje binárně.

## Provedení
Použití Timer0 s prescale 1024 a 32 bit intiger pro čas. Tím je dosaženo přesnoti 0,000016 sekundy.

---
     Nápad: Mr. Q
     Realizace: Viktor Bohuněk
