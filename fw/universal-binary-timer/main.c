/**
    Universal binary timer
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//Timeout, formula: 16000000/1024 * seconds
#define TIMEOUT 15625

#define LED_PLUS  led |= 1 << c;
#define LED_MINUS led &= ~(1 << c);
#define LED_INIT 0

/*
#define LED_PLUS  led &= ~(1 << c);
#define LED_MINUS led |= 1 << c;
#define LED_INIT 255
*/

volatile uint8_t i, c = 0, dir = 0, led = LED_INIT, data_shift_out;
volatile uint32_t counter = 0;

void shiftOut(uint8_t data) {
    data_shift_out = 0;
    if(data & 0b00000001) {
        data_shift_out |= REAL_LED_1;
    }
    if(data & 0b00000010) {
        data_shift_out |= REAL_LED_2;
    }
    if(data & 0b00000100) {
        data_shift_out |= REAL_LED_3;
    }
    if(data & 0b00001000) {
        data_shift_out |= REAL_LED_4;
    }
    if(data & 0b00010000) {
        data_shift_out |= REAL_LED_5;
    }
    if(data & 0b00100000) {
        data_shift_out |= REAL_LED_6;
    }

    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data_shift_out & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM;
    if(counter == TIMEOUT) {
        if(dir == 0) {
            LED_PLUS
            if(c == 5) {
                dir = 1;
            } else {
                c++;
            }
        } else {
            LED_MINUS
            if((c == 0)) {
                dir = 0;
            } else {
                c--;
            }
        }
        shiftOut(led);
        counter = 0;
    }
    counter++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0

    // Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable int
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable ints
    sei();

    shiftOut(led);

    while(1) {
    }

    return 0;
}
