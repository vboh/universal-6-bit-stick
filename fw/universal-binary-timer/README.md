# Universal-binary-timer

## Popis
Firmware umí odpočítávat 6 úseků po stanovené délce periody.

## Provedení
Použití Timer0 s prescale 1024 a 32 bit intiger pro počítání délky úseku. Tím je dosaženo přesnoti 0,000064 sekundy.
Po dosažení stanovené časové konstanty dojde k rozsvícení/zhasnutí další LED. Na koci cyklu (všechny LED svítí/nesvítí)
dojde k obrácení směru.

---
     Nápad: Mr. Q
     Realizace: Viktor Bohuněk
