/**
    Universal-6-independent-bit-timer
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//LED timeouts, formula: 16000000/1024 * seconds

#define LED1_TIMEOUT 3906
#define LED2_TIMEOUT 7812
#define LED3_TIMEOUT 11718
#define LED4_TIMEOUT 15625
#define LED5_TIMEOUT 31250
#define LED6_TIMEOUT 62500

//Variables for counting LED timeout
volatile uint32_t   led1 = 0,
                    led2 = 0,
                    led3 = 0,
                    led4 = 0,
                    led5 = 0,
                    led6 = 0;

                    //Shiftout data; counter in for
volatile uint8_t data = 0, i;

//Shift data to register
void shiftOut(uint8_t data) {
    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

//Every tick of timer
ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM; //Set CTC bottom
    if(led1 == LED1_TIMEOUT) { //If timeout
        data ^= REAL_LED_1; //Switch state of LED
        led1 = 0; //Reset timeout counter
    }
    if(led2 == LED2_TIMEOUT) {
        data ^= REAL_LED_2;
        led2 = 0;
    }
    if(led3 == LED3_TIMEOUT) {
        data ^= REAL_LED_3;
        led3 = 0;
    }
    if(led4 == LED4_TIMEOUT) {
        data ^= REAL_LED_4;
        led4 = 0;
    }
    if(led5 == LED5_TIMEOUT) {
        data ^= REAL_LED_5;
        led5 = 0;
    }
    if(led6 == LED6_TIMEOUT) {
        data ^= REAL_LED_6;
        led6 = 0;
    }
    led1++; //Increment timeout counter
    led2++;
    led3++;
    led4++;
    led5++;
    led6++;
    shiftOut(data); //Shift out data to regoster
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0
    // Prescale 1024 - USED
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable interrupt
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable interrupts
    sei();

    while(1) {
        //NOTHING :D
    }

    return 0;
}
