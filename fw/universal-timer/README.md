# Universal-timer

## Popis
Firmware umí rozblikat každou LED jinou frekvencí určenou časovým úsekem.

## Provedení
Použití Timer0 s prescale 1024 a 32 bit intiger pro každou LED. Tím je dosaženo přesnoti 0,000064 sekundy.
Po dosažení stanovené časové konstanty dojde k přepnutí stavu LED.

---
     Nápad: Mr. Q
     Realizace: Viktor Bohuněk
