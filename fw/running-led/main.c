/**
    Running LED
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

//CTC mode of timer - length 1 tick
#define TIMER_BOTTOM 255

//Mapping LEDs to position in shift register
#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

//Timeout, formula: 16000000/1024 * seconds
#define TIMEOUT 3000//15625

volatile uint8_t i, c = 1, dir = 0;
volatile uint32_t counter = 0;

uint8_t LEDS[7] = {
    0, REAL_LED_1, REAL_LED_2, REAL_LED_3, REAL_LED_4, REAL_LED_5, REAL_LED_6
};

void shiftOut(uint8_t data) {
    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    TCNT0 = TIMER_BOTTOM;
    if(counter == TIMEOUT) {
        //shiftOut(LEDS[c]);

        shiftOut(0xFF & (~LEDS[c]));

        if(dir == 0) {
            c++;
        } else {
            c--;
        }
        if(c == 7) {
            c = 5;
            dir = !dir;
        } else if(c == 0) {
            c = 2;
            dir = !dir;
        }
        counter = 0;
    }
    counter++;
}

int main(void) {
    //Init
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Init timer0

    // Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Enable int
    TIMSK = 0b00000010; //TOIE0
    //Bottom of timer
    TCNT0 = TIMER_BOTTOM;

    //Enable ints
    sei();

    while(1) {
    }

    return 0;
}
