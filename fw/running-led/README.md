# Running LED

## Popis
Firmware umí prohánět zhasnutou(na svítím podkladu)/rozsvícenou(na zhasnutém podkladu) LED přes celý displej.

## Provedení
Použití Timer0 s prescale 1024 a 32 bit intiger pro časový interval mezi přesuny. Tím je dosaženo přesnoti 0,000064 sekundy.
Po dosažení stanovené časové konstanty dojde k posunutí LEDky na displeji. Na konci cyklu (LED zcela vpravo/vlevo) se mění směr.

## Nápad
K vytvoření programu došlo omylem, po chybné interpretaci instrukcí Mr. Q k programu Universal-binary-timer.

---
     Nápad: Viktor Bohuněk
     Realizace: Viktor Bohuněk
