# Morse code

## Popis
Firmware umí zobrazit pole hodnot o délce 8 bitů jako běžící řadu rozsvícených a zhasnutých led.
Původním záměrem je zobrazování morseovky s textem Vánočního přání.

## Provedení
Použití Timer0 s prescale 1024 který je doplněn o SW čítač, nastavený na dobu jedné sekundy.
Každou sekundu tedy proběhne kód, který "posunuje" řadu.

---
     Nápad: Mr. Q
     Realizace: Viktor Bohuněk
