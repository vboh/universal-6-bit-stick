/**
    Morse code
    Copyright (C) 2016 Viktor Bohunìk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//6012 B are free

//Control signals SN74HC595
#define DS_ON   0b00000100
#define SHCP_ON 0b00000001
#define STCP_ON 0b00000010

#define REAL_LED_6 0b00000010
#define REAL_LED_5 0b00001000
#define REAL_LED_4 0b00100000
#define REAL_LED_3 0b00000100
#define REAL_LED_2 0b00010000
#define REAL_LED_1 0b01000000

volatile uint8_t i, time_counter = 0, shift_data = 0, output_data = 0, bit = 0;
volatile uint16_t byte = 0;

#define DATA_LENGTH 3
static const uint8_t DATA[DATA_LENGTH] PROGMEM = {
        0b00110001,0b01010101,0b00000000
    };

/**
Mapuje LED zapsané v bytu stylem první LED na zařízení = bit s váhou 1
Př.:
Vstup 0b00000001 převede na 0b01000000
*/
uint8_t remap_leds(uint8_t data) {
    output_data = 0;
    if(data & 0b00000001) { //Je na pozici označené jedničkou jednička i ve vstupním bitu?
        output_data = REAL_LED_1;
    }

    if(data & 0b00000010) {
        output_data |= REAL_LED_2;
    }

    if(data & 0b00000100) {
        output_data |= REAL_LED_3;
    }

    if(data & 0b00001000) {
        output_data |= REAL_LED_4;
    }

    if(data & 0b00010000) {
        output_data |= REAL_LED_5;
    }

    if(data & 0b00100000) {
        output_data |= REAL_LED_6;
    }
    return output_data;
}

void shiftOut(uint8_t data) {

    data = remap_leds(data); //Provedeme přemapování LEDek

    PORTB = 0x00; // OFF
    for(i = 0; i < 8; i++) {
        PORTB = 0x00; // OFF
        if(data & (1 << i)) {
            PORTB = DS_ON; //DS ON
        } else {
            PORTB = 0x00; // OFF
        }
        PORTB = SHCP_ON;
    }
    PORTB = STCP_ON;
    PORTB = 0x00; // OFF
}

ISR(TIM0_OVF_vect ) {
    if(time_counter == 62) { //Odměříme cca. sekundu
        if((bit < 6) && (byte == 0)) { //Prvních šest bitů celé sekvence si vyžaduje speciální přístup
            //Z prostoru programové paměti přečtme byte a provedem posun, ten zajistí hladký náběh sekvence
            shift_data = (pgm_read_byte(&(DATA[byte])) >> (7 - bit));
        } else { //Ostatní byty
            shift_data = (shift_data << 1); //Posuneme to co už je na
            //Operace zjistí jestli v bitu, který se bude nově zpbrazovat je jednička
            if(pgm_read_byte(&(DATA[byte])) & (0x80 >> bit)) {
                shift_data |= 0b00000001; //Pokud je, na konec sekvece se přidá jednička
            }
        }
        bit++; //Další bit prosím
        if(bit == 8) { //Maximum je sedm...
            bit = 0;
            byte++; //Nový byte
            if(byte == DATA_LENGTH) { //Víc než maximum jich nemáme
                byte = 0;
            }
        }
        shiftOut(shift_data); //Pošleme novou skevenci na zobrazovadlo
        time_counter = 0; //Čas budeme počítat od nuly
    }
    time_counter++;
}

int main(void) {
    //Inicializace pinů
    DDRB = 0b11111111; // PB0, PB1, PB2

    //Inicializace čítače 0

    //Prescale 1024
    TCCR0B = 0b00000101; // CS02 CS00

    //Povolit přerušení časovače
    TIMSK = 0b00000010; //TOIE0

    //Povolit přerušení
    sei();

    while(1) {
    }

    return 0;
}
